// 1. Опишіть, як можна створити новий HTML тег на сторінці.
//     const newTag = document.createElement("my-new-tag");
//     newTag.textContent = "контент тега!";
//     const appDiv = document.getElementById("app");
//     appDiv.appendChild(newTag);
//  Создаем новый елемент HTML тег с помощью document.createElement() потом добавляем содержание.
//  потом нашли елемент <div> с индефикатором "app" и добавили к нему наш новый созданный тег.
// 2. Опишіть, що означає перший параметр функції insertAdjacentHTML і опишіть можливі варіанти цього параметра.
// Функция insertAdjacentHTML является методом работы с DOM-элементами в JavaScript. Она позволяет добавлять HTML-код на страницу в определенном контексте (позиции) относительно заданного DOM-элемента.
// Параметры:
// position: Это обязательный параметр, указывающий контекст, в котором будет вставлен HTML-код. Варианты возможных значений для position:
// "beforebegin": Вставляет HTML-код перед заданным элементом как соседний элемент.
// "afterbegin": Вставляет HTML-код внутрь в заданный элемент, перед первым дочерним элементом.
// "beforeend": Вставляет HTML-код внутрь заданного элемента, после последнего дочернего элемента.
// "afterend": Вставляет HTML-код после заданного элемента как соседний элемент.
// text: Это обязательный параметр, который представляет HTML-код, который нужно вставить в страницы DOM. Он может быть строкой, содержащей допустимый HTML-код или фрагментами HTML.
// 3. Як можна видалити елемент зі сторінки?
// remove()
// removeChild()





function renderList(array, parent = document.body) {
    const listElement = document.createElement("ul");
    parent.appendChild(listElement);

    array.forEach(item => {
        const listItem = document.createElement("li");

        if (Array.isArray(item)) {
            renderList(item, listItem); // Виклик рекурсивної функції для вкладеного масиву
        } else {
            listItem.textContent = item;
        }

        listElement.appendChild(listItem);
    });
}

const data = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];


renderList(data);


setTimeout(() => {
    document.body.innerHTML = "";
}, 3000);

